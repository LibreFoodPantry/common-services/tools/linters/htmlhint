# htmlhint

[htmlhint](https://github.com/htmlhint/HTMLHint) is a
static code analysis tool for HTML.

To enable htmlhint in your project's pipeline add
`htmlhint` to the `ENABLE-JOBS`
variable in your project's `.gitlab-ci.yaml` e.g.:

```bash
variables:
  ENABLE_JOBS: "htmlhint"
```

The ENABLE_JOBS variable is a comma-separated string value, eg.
`ENABLE_JOBS="job1,job2,job3"`

## Using in Visual Studio Code

Install [htmlhint](https://marketplace.visualstudio.com/items?itemName=mkaufman.HTMLHint)
for Visual Studio Code.

## Using Locally in Your Development Environment

To run a single command from the context of the docker image, run the following:

```bash
docker run -it -v `pwd`:/workspace ghcr.io/cardboardci/htmlhint:edge
htmlhint --version
```

## Configuration

[htmlhint configuration](https://github.com/htmlhint/HTMLHint#-configuration)
should be implemented if your project includes html files.

## Licensing

- Code is licensed under [GPL 3.0](documentation/licenses/gpl-3.0.txt).
- Content is licensed under
  [CC-BY-SA 4.0](documentation/licenses/cc-by-sa-4.0.md).
- Developers sign-off on [DCO 1.0](documentation/licenses/dco.txt)
  when they make contributions.
